%global _empty_manifest_terminate_build 0
%global srcname pyroute2
Name:		python-pyroute2
Version:	0.7.9
Release:	2
Summary:	Python Netlink library
License:	GPL-2.0-or-later and Apache-2.0
URL:		https://github.com/svinota/pyroute2
Source0:	https://files.pythonhosted.org/packages/f9/f4/5aa161de059353367a3505a8647f475d11f5ce2af4d2375651a2166a1ef3/pyroute2-0.7.9.tar.gz
BuildArch:	noarch
Patch1:         0001-add-system-call-of-setns-at-sw_64.patch

%description
Pyroute2 is a pure Python **netlink** library. The core requires only Python
stdlib, no 3rd party libraries. The library was started as an RTNL protocol
implementation, so the name is **pyroute2**, but now it supports many netlink
protocols.

%package -n python3-pyroute2
Summary:	Python Netlink library
Provides:	python-pyroute2 = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
BuildRequires:	python3-wheel
Requires:       python3-psutil
%description -n python3-pyroute2
Pyroute2 for python3 is a pure Python **netlink** library. The core requires only
Python stdlib, no 3rd party libraries. The library was started as an RTNL protocol
implementation, so the name is **pyroute2**, but now it supports many netlink
protocols.

%package help
Summary:	Development documents and examples for pyroute2
Provides:	python3-pyroute2-doc
%description help
Development documents and examples for pyroute2.

%prep
%autosetup -n pyroute2-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi

%files -n python3-pyroute2
%license LICENSE*
%doc README*
%{_bindir}/ss2
%{_bindir}/%{srcname}-cli
%{_bindir}/%{srcname}-dhcp-client
%{_bindir}/%{srcname}-test-platform
%{python3_sitelib}/pr2modules
%{python3_sitelib}/pyroute2
%{python3_sitelib}/pyroute2*.dist-info/

%files help
%{_docdir}/*

%changelog
* Thu Oct 19 2023 zhangxianting <zhangxianting@uniontech.com> - 0.7.9-2
- Add system call of setns at sw_64

* Mon Jul 17 2023 Dongxing Wang <dxwangk@isoftstone.com> - 0.7.9-1
- Update package to version 0.7.9

* Fri May 05 2023 wangkai <13474090681@163.com> - 0.7.3-3
- Compling package with pyproject

* Fri Dec 09 2022 liukuo <liukuo@kylinos.cn> - 0.7.3-2
- License compliance rectification

* Mon Dec 05 2022 liqiuyu <liqiuyu@kylinos.cn> - 0.7.3-1
- Update package to version 0.7.3

* Fri Oct 23 2020 wutao <wutao61@huawei.com> - 0.5.14-1
- upgrade and disable python2 build

* Tue May 12 2020 hexiaowen <hexiaowen@huawei.com> - 0.5.3-6
- init packaging
